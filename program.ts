import * as pm2 from 'pm2';
import * as provider from './http-provider';
import * as Config from './config';
import {createContext} from "./src/provider/context";

pm2.connect(fault => {
  if (fault) {
    Config.out(fault);
    process.exit(2);
  }

  const context = createContext();
  provider.init(Config.port, Config.out, Config.functionCallLiteral, context);

  pm2.start(
    {
      script: 'worker.ts',
      exec_mode: 'cluster',
      instances: 2,
      interpreter: Config.tsNodePath
    },
    fault => {
      if (fault) {
        throw fault;
      }
    }
  );
});
