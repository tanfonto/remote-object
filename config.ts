export const port = 3000;
export const tsNodePath = '/usr/local/bin/ts-node';
export const out = console.log;
export const pollingInterval = 5000;
export const functionCallLiteral = '()';
