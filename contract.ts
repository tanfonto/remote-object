export type Contract = {
  prop: number;
  method: () => number;
  nested: { method: () => number; asyncMethod: () => Promise<number> };
  big: bigint;
  symbol: symbol;
  nullable: string | null;
  notHere: string | undefined;
};
