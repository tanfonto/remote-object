import fetch from 'node-fetch';
import { Descriptor, KnownType, TypeToLiteral } from './src/types';

export const httpProxy = (url: string) =>
  async function get<T extends KnownType>(
    path: readonly string[] = []
  ): Promise<Descriptor<TypeToLiteral<T>>> {
    return fetch(path.length ? `${url}/${path.join('/')}` : url)
      .then(res => res.json())
      .then(JSON.parse)
      .catch(value => ({ type: 'error', value }));
  };
