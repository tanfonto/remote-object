import { provide } from './src/provider/provide';
import * as express from 'express';
import { resolvePath } from './src/provider/resolve-path';
import { KnownType } from './src/types';

export const init = <T extends KnownType>(
  port: number,
  out = console.log,
  functionCallLiteral: string,
  context: T
): void => {
  const server = express();
  const resolve = resolvePath(functionCallLiteral);
  const provider = provide(context, resolve);

  server.use(express.json());

  server.get('*', async (req, res) => {
    const segments = req.path === '/' ? [] : req.path.split('/');
    const path = segments.slice(1, segments.length);
    res.json(await provider(path));
  });

  server.listen(port);

  out(`http server running on port ${port}`);
};
