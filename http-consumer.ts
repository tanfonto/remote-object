import { consume } from './src/consumer/consume';
import { httpProxy } from './http-proxy';
import { Contract } from './contract';

export const init = async (
  url: string,
  functionCallLiteral: string,
  out = console.log
): Promise<void> => {
  const remote = await consume<Contract>(httpProxy(url), functionCallLiteral);
  out(remote);
  out(await remote.prop);
  out(remote.method);
  out(await remote.method());
  out(await remote.big);
  out(await remote.symbol);
  out(await remote.nullable);
  out(await remote.notHere);
  const nested = await remote.nested;
  out(nested);
  out(await nested.method());
  out(await nested.asyncMethod());
};
