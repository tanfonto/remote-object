# remote-object

### Prerequisites:

* Yarn;
* PM2 (installed globally);

### Running

* `pm2 start main.js && pm2 logs` (workers will periodically query the server and output remote object details to the console);
* navigate to `http://localhost:3000/` and build a query of choice by defining url segments (use `()` literal to call a function), i.e:
  * `http://localhost:3000/prop` to retrieve a descriptor for `root.prop`;
  * `http://localhost:3000/nested/method` to retrieve a function descriptor of `root.nested.method`;
  * `http://localhost:3000/nested/method/()` to call the function and retrieve its return value wrapped in a relevant descriptor;

TODO: 

* Error handling;
* Tests;
* More specific typings;
