import * as pm2 from 'pm2';
import * as Config from './config';
import * as consumer from './http-consumer';

pm2.connect(fault => {
  if (fault) {
    Config.out(fault);
    process.exit(2);
  }

  const url = `http://localhost:${Config.port}`;

  setInterval(
    async () =>
      await consumer.init(url, Config.functionCallLiteral, Config.out),
    Config.pollingInterval
  );
});
