import { KnownType } from '../types';

export const resolvePath = (functionCallLiteral: string) =>
  async function resolve<T extends KnownType>(
    source: T,
    [head, ...tail]: readonly string[] = []
  ): Promise<KnownType> {
    switch (typeof source) {
      case 'function':
        return head === functionCallLiteral
          ? resolve(await source(), tail)
          : Error(`${source} is a function`);
      case 'object':
        return source && head
          ? head === functionCallLiteral
            ? Error(`${source} is not a function`)
            : resolve(source[head], tail)
          : source;
      default:
        return tail.length ? Error(`${source} is not an object`) : source;
    }
  };
