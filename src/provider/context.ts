import { Contract } from '../../contract';

export const createContext = (): Contract => ({
  prop: 12,
  method: function() {
    return 14;
  },
  nested: { method: () => 60, asyncMethod: () => Promise.resolve(50) },
  big: BigInt(9007199254740991),
  symbol: Symbol('Definitely a symbol'),
  nullable: null,
  notHere: undefined
});
