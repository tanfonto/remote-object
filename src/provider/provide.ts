import { describe } from './descriptors';
import { JsonString, KnownType } from '../types';

export const provide = <T extends KnownType>(
  source: T,
  resolvePath: (source: T, path: readonly string[]) => Promise<KnownType>
) => async (path: readonly string[] = []): Promise<JsonString> =>
  JSON.stringify(describe(await resolvePath(source, path)));
