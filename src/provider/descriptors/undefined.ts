import { UndefinedDescriptor } from '../../types';

export const undefinedDescriptor = (_: undefined): UndefinedDescriptor => ({
  type: 'undefined',
  value: '[undefined]'
});
