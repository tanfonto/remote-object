import { symbolDescriptor } from './symbol';
import {
  Descriptor,
  KnownType,
  KnownTypeLiteral,
  ObjectType,
  TypeToLiteral
} from '../../types';
import { functionDescriptor } from './function';
import { entries } from '../../entries';
import { bigintDescriptor } from './bigint';
import { nullDescriptor } from './null';
import { serializableTypeDescriptor } from './serializable';
import { undefinedDescriptor } from './undefined';
import { errorDescriptor } from './error';

const objectDescriptor = <T extends ObjectType>(
  source: T
): Descriptor<'object'> => ({
  type: 'object',
  value: entries(source).reduce(
    (acc, [key, value]) => ({
      ...acc,
      [key]: describe(value)
    }),
    Object.create(null)
  )
});

const descriptors = new Map<
  KnownTypeLiteral,
  (value: KnownType) => Descriptor<TypeToLiteral<typeof value>>
>([
  ['symbol', symbolDescriptor],
  ['function', functionDescriptor],
  ['bigint', bigintDescriptor],
  ['null', nullDescriptor],
  ['undefined', undefinedDescriptor],
  ['object', objectDescriptor],
  ['string', serializableTypeDescriptor],
  ['number', serializableTypeDescriptor],
  ['boolean', serializableTypeDescriptor],
  ['error', errorDescriptor]
]);

const asKnownType = <T extends KnownType>(source: T): KnownTypeLiteral => {
  const type = typeof source;
  if (type !== 'object') return type;
  if (source === null) return 'null';
  if (source instanceof Error) return 'error';
  return 'object';
};

export const describe = <T extends KnownType>(
  value: T
): Descriptor<TypeToLiteral<T>> => {
  const key = asKnownType(value);
  const descriptor = descriptors.get(key);

  return descriptor!(value) as Descriptor<TypeToLiteral<T>>;
};
