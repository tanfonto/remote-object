import { Descriptor } from '../../types';

export const errorDescriptor = (value: Error): Descriptor<'error'> => ({
  type: 'error',
  value
});
