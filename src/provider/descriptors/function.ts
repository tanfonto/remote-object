import { FunctionDescriptor } from '../../types';

export const functionDescriptor = <T extends Function>(
  _: T
): FunctionDescriptor => ({
  type: 'function',
  value: '[Function]'
});
