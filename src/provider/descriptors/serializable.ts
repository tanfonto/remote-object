import { Descriptor, SerializableType, TypeToLiteral } from '../../types';

export const serializableTypeDescriptor = <T extends SerializableType>(
  value: T
): Descriptor<TypeToLiteral<T>> =>
  ({
    type: typeof value,
    value
  } as any);
