import { SymbolDescriptor } from '../../types';

export const symbolDescriptor = (source: symbol): SymbolDescriptor => ({
  type: 'symbol',
  value: `Symbol (${source.description})`
});
