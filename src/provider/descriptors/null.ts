import {NullDescriptor} from '../../types';

export const nullDescriptor = (_: null): NullDescriptor => ({
  type: 'null',
  value: null
});
