import { BigintDescriptor } from '../../types';

export const bigintDescriptor = (source: bigint): BigintDescriptor => ({
  type: 'bigint',
  value: source.toString()
});
