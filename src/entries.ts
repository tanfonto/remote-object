import { ObjectType } from './types';

export const entries: <T extends ObjectType>(
  source: T
) => [keyof T & string, T[keyof T]][] = Object.entries;
