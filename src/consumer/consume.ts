import * as Parser from './parser';
import { ObjectType, Proxy, RemoteObject } from '../types';

export const consume = async <T extends ObjectType>(
  proxy: Proxy,
  functionCallLiteral: string
): RemoteObject<T> => {
  const schema = await proxy<T>();
  const parse = Parser.create(proxy, functionCallLiteral);
  return parse(schema) as any;
};
