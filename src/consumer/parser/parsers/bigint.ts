import { BigintDescriptor } from '../../../types';

export const bigintParser = ({ value }: BigintDescriptor) => BigInt(value);
