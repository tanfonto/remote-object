import { Descriptor, SerializableTypeLiteral } from '../../../types';

export const deserializableParser = ({
  value
}: Descriptor<SerializableTypeLiteral | 'null'>): typeof value => value;
