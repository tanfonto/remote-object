import {
  Descriptor,
  ErrorDescriptor,
  KnownType,
  KnownTypeLiteral,
  ObjectType,
  ObjectValue,
  Proxy,
  TypeToLiteral
} from '../../types';
import { entries } from '../../entries';
import { undefinedParser } from './parsers/undefined';
import { deserializableParser } from './parsers/deserializable';
import { bigintParser } from './parsers/bigint';

const parsers = new Map<
  KnownTypeLiteral,
  (value: Descriptor<KnownTypeLiteral>) => KnownType
>([
  ['undefined', undefinedParser],
  ['bigint', bigintParser],
  ['number', deserializableParser],
  ['string', deserializableParser],
  ['boolean', deserializableParser],
  ['null', deserializableParser],
  ['symbol', deserializableParser]
]);

const objectParser = <T extends ObjectValue<ObjectType>>(
  source: T,
  project: (
    key: keyof T & string,
    descriptor: Descriptor<KnownTypeLiteral>
  ) => any
) =>
  Object.defineProperties(
    {},
    entries(source).reduce(
      (acc, [key, descriptor]) => ({
        ...acc,
        [key]: {
          enumerable: true,
          get: () => project(key, descriptor)
        }
      }),
      {}
    )
  );

export const create = (proxy: Proxy, functionCallLiteral: string) => {
  return function parse<T extends KnownType>(
    { type, value }: Descriptor<TypeToLiteral<T>> | ErrorDescriptor,
    path: readonly string[] = []
  ): Promise<KnownType> | (() => Promise<KnownType>) {
    switch (type) {
      case 'error':
        return Promise.resolve(value);
      case 'function':
        return () => proxy([...path, functionCallLiteral]).then(parse);
      case 'object':
        return Promise.resolve(
          objectParser(value as any, (key, descriptor) => {
            const newPath = [...path, key];
            return descriptor.type === 'object'
              ? proxy(newPath).then(descriptor => parse(descriptor, newPath))
              : parse(descriptor, newPath);
          })
        );
      default:
        return Promise.resolve(parsers.get(type)!({ type, value } as any));
    }
  };
};
