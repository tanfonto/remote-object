export type SerializableType = number | string | boolean;

export type SerializableTypeLiteral = 'number' | 'string' | 'boolean';

export type Primitive = SerializableType | symbol | bigint | null;

export type ObjectType = {
  [key: string]: KnownType;
};

export type KnownType =
  | ObjectType
  | Primitive
  | Function
  | undefined
  | null
  | Error;

export type KnownTypeLiteral =
  | SerializableTypeLiteral
  | 'bigint'
  | 'symbol'
  | 'object'
  | 'function'
  | 'undefined'
  | 'null'
  | 'error';

export type TypeToLiteral<T extends KnownType> = T extends number
  ? 'number'
  : T extends string
  ? 'string'
  : T extends boolean
  ? 'boolean'
  : T extends null
  ? 'null'
  : T extends undefined
  ? 'undefined'
  : T extends bigint
  ? 'bigint'
  : T extends symbol
  ? 'symbol'
  : T extends Function
  ? 'function'
  : T extends ObjectType
  ? 'object'
  : T extends Error
  ? 'error'
  : never;

export type LiteralToType<T extends KnownTypeLiteral> = T extends 'number'
  ? number
  : T extends 'string'
  ? string
  : T extends 'boolean'
  ? boolean
  : T extends 'null'
  ? null
  : T extends 'undefined'
  ? undefined
  : T extends 'bigint'
  ? bigint
  : T extends 'symbol'
  ? symbol
  : T extends 'function'
  ? Function
  : T extends 'object'
  ? ObjectType
  : T extends 'error'
  ? Error
  : never;

export type TypedDescriptor<T extends KnownTypeLiteral> = { type: T };

export type SerializableTypeDescriptor<
  T extends SerializableTypeLiteral
> = TypedDescriptor<T> & {
  value: LiteralToType<T>;
};

export type FunctionDescriptor = TypedDescriptor<'function'> & {
  value: '[Function]';
};

export type UndefinedDescriptor = TypedDescriptor<'undefined'> & {
  value: '[undefined]';
};

export type NullDescriptor = TypedDescriptor<'null'> & {
  value: null;
};

export type SymbolDescriptor = TypedDescriptor<'symbol'> & { value: string };

export type BigintDescriptor = TypedDescriptor<'bigint'> & { value: string };

export type Descriptor<T extends KnownTypeLiteral> = T extends 'function'
  ? FunctionDescriptor
  : T extends 'object'
  ? ObjectDescriptor<T & ObjectType>
  : T extends 'undefined'
  ? UndefinedDescriptor
  : T extends 'null'
  ? NullDescriptor
  : T extends 'symbol'
  ? SymbolDescriptor
  : T extends 'bigint'
  ? BigintDescriptor
  : T extends SerializableTypeLiteral
  ? SerializableTypeDescriptor<T>
  : T extends 'error'
  ? ErrorDescriptor
  : never;

export type ErrorDescriptor = { type: 'error'; value: Error };

export type ObjectValue<T extends ObjectType> = {
  [K in keyof T & string]: Descriptor<TypeToLiteral<T[K]>>;
};

export type ObjectDescriptor<T extends ObjectType> = TypedDescriptor<
  'object'
> & {
  value: ObjectValue<T>;
};

export type Proxy = <T extends KnownType>(
  path?: readonly string[]
) => Promise<Descriptor<TypeToLiteral<T>>>;

export type JsonString = string;

type UnwrapPromise<T> = T extends Promise<infer U> ? Promise<U> : Promise<T>;

export type RemoteObject<T extends ObjectType> = Promise<
  {
    [K in keyof T]: T[K] extends () => infer U
      ? () => UnwrapPromise<U>
      : T[K] extends ObjectType
      ? RemoteObject<T[K]>
      : UnwrapPromise<T[K]>;
  }
>;
